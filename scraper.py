import sys

import requests
from bs4 import BeautifulSoup
import cloudscraper
import pickle
import os


# Class for scraping KenPom data
class KenPom:
    def __init__(self, year):
        # Saved data to a pickle file to prevent access errors when repeatedly scraping websites
        path = f"kenpom/{year}.pickle"
        if not os.path.exists(path):
            self._url = "https://kenpom.com/index.php?y=" + str(year)
            self._scraper = cloudscraper.create_scraper(
                browser={
                    'browser': 'firefox',
                    'platform': 'windows',
                    'mobile': False
                }
            )
            # Parse HTML and grab useful columns
            self._page = self._scraper.get(self._url)
            self._soup = BeautifulSoup(self._page.content, 'html.parser')
            self._rank = self._soup.select("table#ratings-table tbody tr td:nth-child(5)")
            self._sos = self._soup.select("table#ratings-table tbody tr td:nth-child(14)")
            _team = self._soup.select("table#ratings-table tbody tr .next_left")
            self._team_names = []

            for team in _team:
                self._team_names.append(format_team_name(team.text))

            self._team_names = [i for i in self._team_names if i != 'team' and i != '']

            self._team_to_rank = self.get_team_to_rank()

            with open(path, 'wb') as file:
                pickle.dump(self._team_to_rank, file)
        else:
            with open(path, 'rb') as file:
                self._team_to_rank = pickle.load(file)

    # Get the dictionary that maps team names to their overall efficiency
    def get_team_to_rank(self):
        if not hasattr(self, '_team_to_rank'):
            team_to_rank = {}
            for i, team_name in enumerate(self._team_names):
                rank = _get_rank(self._rank[i].text)
                team_to_rank[team_name] = rank

            return team_to_rank
        return self._team_to_rank


# Class for scraping sports reference data
class TeamRecord:
    def __init__(self, school, year):
        sys.setrecursionlimit(1000000)  # Required for saving very large scraping data
        # Saved data to a pickle file to prevent access errors when repeatedly scraping websites
        path = f"records/{school}-{year}.pickle"
        if not os.path.exists(path):
            self._url = "https://www.sports-reference.com/cbb/schools/" + school + "/" + str(year) + "-schedule.html"
            self._page = requests.get(self._url)
            self._soup1 = BeautifulSoup(self._page.content, 'html.parser')
            with open(path, 'wb') as file:
                pickle.dump(self._soup1, file)
        else:
            with open(path, 'rb') as file:
                self._soup1 = pickle.load(file)

        # Extract useful columns of data
        if year >= 2015:
            self._WL = self._soup1.select("table#schedule tbody tr td:nth-child(9)")  # win-loss
            self._opps = self._soup1.select("table#schedule tbody tr td:nth-child(6)")  # opponents
            self._site = self._soup1.select("table#schedule tbody tr td:nth-child(5)")  # location
            self._pf = self._soup1.select("table#schedule tbody tr td:nth-child(10)")  # points for
            self._pa = self._soup1.select("table#schedule tbody tr td:nth-child(11)")  # points against
            self.team = format_team_name(school)
        else:
            self._WL = self._soup1.select("table#schedule tbody tr td:nth-child(8)")  # win-loss
            self._opps = self._soup1.select("table#schedule tbody tr td:nth-child(5)")  # opponents
            self._site = self._soup1.select("table#schedule tbody tr td:nth-child(4)")  # location
            self._pf = self._soup1.select("table#schedule tbody tr td:nth-child(9)")  # points for
            self._pa = self._soup1.select("table#schedule tbody tr td:nth-child(10)")  # points against
            self.team = format_team_name(school)

        path = f"records/{school}-{year}-gamelogs.pickle"
        if not os.path.exists(path):
            self._url = f"https://www.sports-reference.com/cbb/schools/{school}/{year}-gamelogs.html"
            self._page = requests.get(self._url)
            self._soup2 = BeautifulSoup(self._page.content, 'html.parser')
            self._fg = self._soup2.select("table#sgl-basic tbody tr td:nth-child(10)")
            self._fta = self._soup2.select("table#sgl-basic tbody tr td:nth-child(15)")
            self._tov = self._soup2.select("table#sgl-basic tbody tr td:nth-child(22)")

            data = (self.get_fg_percentage(), self.get_fta(), self.get_tov())
            with open(path, 'wb') as file:
                pickle.dump(data, file)
        else:
            with open(path, 'rb') as file:
                self._fg_done, self._fta_done, self._tov_done = pickle.load(file)

    # Get the team's win loss record
    def get_win_loss(self):
        results = []  # Full win loss record game-by-game
        for i in range(0, len(self._WL)):
            anchor = self._WL[i]
            s = anchor.text
            results += [s]
        return results

    # Get a listing of the opponents a team played
    def get_opponents(self):
        versus = []  # all opponents in order
        for i in range(0, len(self._opps)):
            anchor = self._opps[i]
            s = anchor.text
            s = format_team_name(s)
            versus += [s]
        return versus

    # Get the field goal percentage per game
    def get_fg_percentage(self):
        if not hasattr(self, '_fg_done'):
            fg_perc = []
            for fg in self._fg:
                fg = fg.text
                fg_perc.append(float(fg))
            return fg_perc
        return self._fg_done

    # Get the free throws attempted per game
    def get_fta(self):
        if not hasattr(self, '_fta_done'):
            ft_perc = []
            for ft in self._fta:
                ft = ft.text
                ft_perc.append(int(ft))
            return ft_perc
        return self._fta_done

    # Get the turnovers per game
    def get_tov(self):
        if not hasattr(self, '_tov_done'):
            tov = []
            for t in self._tov:
                tov.append(int(t.text))
            return tov
        return self._tov_done

    # Get the game locations
    def get_game_locations(self):
        locs = []  # Locations of each game
        for i in range(0, len(self._site)):
            anchor = self._site[i]
            s = anchor.text
            if s == "":
                locs += ["H"]
            elif s == "@":
                locs += ["A"]
            else:
                locs += [s]
        return locs

    # Get the margins of victory for each game played
    def get_victory_margins(self):
        margin = []  # positive = won, negative = lost
        for i in range(0, len(self._pf)):
            a, b = self._pf[i], self._pa[i]
            s1, s2 = a.text, b.text
            diff = int(s1) - int(s2)
            margin += [diff]
        return margin


# Format team name and handle edge cases where the team names do not match between KenPom and SportsReference
def format_team_name(name):
    # Convert team name to all lowercase, no spaces, and remove numbers from web scrape
    name = name.lower()
    name = ''.join(list((filter(lambda x: x.isalpha(), name))))
    name = name.replace("state", "st")  # state edge cases
    name = name.replace(" (FL)", "fl")  # Miami edge case
    if name == "northcarolinast":
        name = "ncstate"  # NC State edge case
    if name == "ncst":
        name = "ncstate"
    if name == "stjohnsny":
        name = "stjohns"
    if name == "centralflorida":
        name = "ucf"
    if name == "saintfrancispa":
        name = "stfrancispa"
    if name == "virginiacommonwealth":
        name = "vcu"
    if name == "nevadalasvegas":
        name = "unlv"
    if name == "louisianast":
        name = "lsu"
    if name == "albanyny":
        name = "albany"
    if name == "purduefortwayne":
        name = "fortwayne"
    if name == "virginiamilitaryinstitute":
        name = "vmi"
    if name == "marylandbaltimorecounty":
        name = "umbc"
    if name == "louisiana":
        name = "louisianalafayette"
    if name == "loyolail":
        name = "loyolachicago"
    if name == "collegeofcharleston":
        name = "charleston"
    if name == "stmarysmd":
        name = "mountstmarys"
    if name == "prairieview":
        name = "prairieviewam"
    if name == "southerncalifornia":
        name = "usc"
    if name == "southernmethodist":
        name = "smu"
    if name == "detroitmercy":
        name = "detroit"
    if name == "brighamyoung":
        name = "byu"
    if name == "floridainternational":
        name = "fiu"
    if name == "kansascity":
        name = "umkc"
    if name == "omaha":
        name = "nebraskaomaha"
    if name == "grambling":
        name = "gramblingst"
    if name == "massachusettslowell":
        name = "umasslowell"
    if name == "houstonchristian":
        name = "houstonbaptist"
    if name == "centralconnecticutst":
        name = "centralconnecticut"
    if name == "southcarolinaupst":
        name = "uscupst"
    if name == "fortwayne":
        name = "umbc"
    if name == "southernmississippi":
        name = "southernmiss"
    if name == "texasriograndevalley":
        name = "utriograndevalley"
    if name == "pennsylvania":
        name = "penn"
    if name == "chaminade":
        name = "idaho"
    if name == "saintjosephsin":
        name = "idaho"
    if name == "tusculum":
        name = "idaho"

    return name


# Format the parsed overall efficiency into a python float
def _get_rank(rank):
    if rank[0] == "+":
        return float(rank[1:])
    else:
        return -float(rank[1:])
