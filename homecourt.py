from time import sleep

from scraper import KenPom, TeamRecord
import numpy as np
import matplotlib.pyplot as plt


# Splits data into three lists based on location (home, away, or neutral)
def split_by_location(sites, data):
    home, away, neutral = [], [], []
    for i, site in enumerate(sites):
        if site == "H":
            home.append(data[i])
        elif site == "A":
            away.append(data[i])
        else:
            neutral.append(data[i])

    return home, away, neutral


# Scrapes all the data we need for a team/year and returns a TeamRecord and KenPom object
def scrape(team, year):
    return TeamRecord(team, year), KenPom(year)


# Computes the efficiency differentials -- returns a list of expected number of points to win by (on average)
def compute_efficiency_differentials(team_record, ken_pom):
    team_efficiency = ken_pom.get_team_to_rank()[team_record.team]

    opponents = team_record.get_opponents()
    efficiency_differentials = []
    for opponent in opponents:
        opponent_efficiency = ken_pom.get_team_to_rank()[opponent]
        efficiency_differentials.append(team_efficiency - opponent_efficiency)

    return efficiency_differentials


# Prints the averages of the three lists
def print_avgs(home, away, neutral):
    print(f"Average home diff: {np.mean(home)}")
    print(f"Average away diff: {np.mean(away)}")
    print(f"Average neutral diff: {np.mean(neutral)}")


# Computes the homecourt advantage for the given team during the given year
def homecourt_advantage(team, year, debug=False):
    # scrape data
    team_record, ken_pom = scrape(team, year)

    # get game locations
    sites = team_record.get_game_locations()

    # computed expected number of points to win by (efficiency differentials)
    eff_diffs = compute_efficiency_differentials(team_record, ken_pom)

    # determine the difference between expected win rate and what actually happened
    eff_change = np.array(team_record.get_victory_margins()) - np.array(eff_diffs)

    # split those differences by game location
    home_change, away_change, neutral_change = split_by_location(sites, eff_change)

    fg = team_record.get_fg_percentage()
    fta = team_record.get_fta()
    tov = team_record.get_tov()
    home_fg, away_fg, _ = split_by_location(team_record.get_game_locations(), fg)
    home_fta, away_fta, _ = split_by_location(team_record.get_game_locations(), fta)
    home_tov, away_tov, _ = split_by_location(team_record.get_game_locations(), tov)

    # compute separate averages for home vs away games
    average_home_diff = np.mean(home_change)
    average_away_diff = np.mean(away_change)

    # determine the homecourt advantage (difference between home and away performance)
    homecourt_adv = average_home_diff - average_away_diff


    # If debug is enabled, print more info
    num_home = len(home_change)
    total = len(eff_change)
    if debug:
        print(f"Percent games at home: {(num_home / total) * 100}%")
        print_avgs(home_change, away_change, neutral_change)

    # return the homecourt advantage
    return homecourt_adv, float(np.mean(home_fg)), float(np.mean(away_fg)), float(np.mean(home_fta)), float(
        np.mean(away_fta)), float(np.mean(home_tov)), float(np.mean(away_tov))


# Average homecourt advantage from 2015-2021
def average_homecourt_advantage(team, start=2015, end=2020):
    avg = 0
    home_fg_avg = 0
    away_fg_avg = 0
    home_fta_avg = 0
    away_fta_avg = 0
    home_tov_avg = 0
    away_tov_avg = 0
    for year in range(start, end):
        print(year)
        sleep(2)
        hca, home_fg, away_fg, home_fta, away_fta, home_tov, away_tov = homecourt_advantage(team, year)
        avg += hca
        home_fg_avg += home_fg
        away_fg_avg += away_fg
        home_fta_avg += home_fta
        away_fta_avg += away_fta
        home_tov_avg += home_tov
        away_tov_avg += away_tov

    return avg / (end - start), home_fg_avg / (end - start), away_fg_avg / (end - start), home_fta_avg / (
                end - start), away_fta_avg / (end - start), home_tov_avg / (end - start), away_tov_avg / (end - start)


# ACC Team Names
Teams = ["boston-college", "clemson", "duke", "florida-state", "georgia-tech",
         "louisville", "miami-fl", "north-carolina", "north-carolina-state", "notre-dame",
         "pittsburgh", "syracuse", "virginia", "virginia-tech", "wake-forest"]

Nice_Team_Names = ["Boston College", "Clemson", "Duke", "Florida State", "Georgia Tech",
                   "Louisville", "Miami", "UNC", "NC State", "Notre Dame",
                   "Pittsburgh", "Syracuse", "Virginia", "Virginia Tech", "Wake Forest"]

# Team to compute home court advantage metrics for
team = 'duke'

hca, home_fg, away_fg, home_fta, away_fta, home_tov, away_tov = average_homecourt_advantage(team, start=2015, end=2020)

print(f"Average {team} homecourt advantage:\n"
      f"\tHome court advantage: {hca:.2f}\n\n"
      f"\tHome fg: {home_fg * 100:.2f}\n"
      f"\tAway fg: {away_fg * 100:.2f}\n\n"
      f"\tHome fta: {home_fta:.2f}\n"
      f"\tAway fta: {away_fta:.2f}\n\n"
      f"\tHome tov: {home_tov:.2f}\n"
      f"\tAway tov: {away_tov:.2f}")


###############################################################################
################################ GRAPHING CODE ################################
###############################################################################

# Averaged between 2015-2019 inclusive
# Computed homecourt advantage for all ACC teams (by running this program for each team)
home_court_advantages = [4.011, 5.574, 8.584, 7.930, 3.468, 5.006, 3.364, 6.438,
                         6.027, 2.838, 4.479, 6.595, 2.770, 4.656, 4.360]

# stadium capacity for each team
stadium_capacity = [8606, 9000, 9314, 11500, 8600, 22090, 7972, 21750,
                    19700, 9149, 12508, 35000, 14593, 10052, 14407]


# Make Graphs
# plt.scatter(stadium_capacity, home_court_advantages)
# plt.xlabel("Stadium capacity")
# plt.ylabel("Home Court Advantage")
# plt.title("Home Court Advantage vs. Stadium Capacity")
#
# # plt.show()
# plt.savefig("stadium_capacity.png")

# plt.clf()
# efficiencies = []
# for team in Teams:
#     eff = 0
#     for i in range(2015, 2020):
#         kenPom = KenPom(i)
#         eff += kenPom.get_team_to_rank()[format_team_name(team)]
#     eff /= 5
#     efficiencies.append(eff)
#
# plt.scatter(efficiencies, home_court_advantages)
# plt.xlabel("Team Efficiency")
# plt.ylabel("Home Court Advantage")
# plt.title("Home Court Advantage vs. Team Efficiency")
# # plt.show()
# plt.savefig("team_efficiency.png")
#
# plt.clf()
# years = range(2000, 2022)
# hca = []
#
# for year in years:
#     advg = homecourt_advantage("duke", year)
#     hca.append(advg)
#
# plt.scatter(years, hca)
# plt.xlabel("Year")
# plt.ylabel("Home Court Advantage")
# plt.title("Duke Home Court Advantage vs. Time")
# plt.savefig("duke_year.png")
# plt.show()


# Make Graphs
# xdat = np.linspace(1, 15, 15)
# fig = plt.figure(num=1, clear=True)
# ax = fig.add_subplot(1,1,1)
# ax.barh(xdat, home_court_advantages)
# ax.set_yticks(xdat)
# ax.set_yticklabels(Nice_Team_Names)
# ax.invert_yaxis()  # labels read top-to-bottom
# ax.set(xlabel="Home Court Advantage", title="Home Court Advantage for ACC Schools")
# #ax.bar_label(home_court_advantages, fmt='%.2f')
# fig.tight_layout()
# fig.savefig("homecourt_by_team.png")
